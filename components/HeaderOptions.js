import { DotsVerticalIcon, LocationMarkerIcon,NewspaperIcon,PhotographIcon,PlayIcon,SearchIcon } from "@heroicons/react/outline"
import HeaderOption from "./HeaderOption"

function HeaderOptions() {
    return (
        <div className="text-sm flex w-full text-gray-500 justify-evenly lg:justify-start lg:space-x-44 lg:pl-44 border-b border-gray-200">
            <div className="flex space-x-6">    
                <HeaderOption Icon={SearchIcon} title="All" selected/>
                <HeaderOption Icon={LocationMarkerIcon} title="Maps"/>
                <HeaderOption Icon={PlayIcon} title="Videos"/>
                <HeaderOption Icon={PhotographIcon} title="Images"/>
                <HeaderOption Icon={NewspaperIcon} title="News"/>
                <HeaderOption Icon={DotsVerticalIcon} title="More"/>
            </div>
            <div className="flex space-x-4">
                <p className="link">Settings</p>
                <p className="link">Tools</p>
            </div>
        </div>
    )
}

export default HeaderOptions
