export default {
    "kind": "customsearch#search",
    "url": {
        "type": "application/json",
        "template": "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json"
    },
    "queries": {
        "request": [
            {
                "title": "Google Custom Search - google",
                "totalResults": "12000000",
                "searchTerms": "google",
                "count": 10,
                "startIndex": 1,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "2fa9b64469e2803b3"
            }
        ],
        "nextPage": [
            {
                "title": "Google Custom Search - google",
                "totalResults": "12000000",
                "searchTerms": "google",
                "count": 10,
                "startIndex": 11,
                "inputEncoding": "utf8",
                "outputEncoding": "utf8",
                "safe": "off",
                "cx": "2fa9b64469e2803b3"
            }
        ]
    },
    "context": {
        "title": "Google"
    },
    "searchInformation": {
        "searchTime": 0.570558,
        "formattedSearchTime": "0.57",
        "totalResults": "12000000",
        "formattedTotalResults": "12,000,000"
    },
    "items": [
        {
            "kind": "customsearch#result",
            "title": "Google AdSense - Earn Money From Website Monetization",
            "htmlTitle": "<b>Google</b> AdSense - Earn Money From Website Monetization",
            "link": "https://www.google.co.in/adsense/start/",
            "displayLink": "www.google.co.in",
            "snippet": "Earn money with website monetization from Google AdSense. We'll optimize your \nad sizes to give them more chance to be seen and clicked.",
            "htmlSnippet": "Earn money with website monetization from <b>Google</b> AdSense. We&#39;ll optimize your <br>\nad sizes to give them more chance to be seen and clicked.",
            "cacheId": "N6WYlUkL3BwJ",
            "formattedUrl": "https://www.google.co.in/adsense/start/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/adsense/start/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROto1J0I_1Ekim0VAQPWn8MZz4wJEjUmI0hvxWhtiZLliH-QuLp1ICznKa",
                        "width": "275",
                        "height": "183"
                    }
                ],
                "metatags": [
                    {
                        "image": "https://lh3.googleusercontent.com/ui3nbG7vgTq2sakCuph87K5w-nojUvsGkyR01xb6zoIosG0nzL5LuteUJ70yzil56lUeEmFWVHb1N3TZRgEAjU6j5J4optMGe9iU",
                        "og:image": "https://lh3.googleusercontent.com/ui3nbG7vgTq2sakCuph87K5w-nojUvsGkyR01xb6zoIosG0nzL5LuteUJ70yzil56lUeEmFWVHb1N3TZRgEAjU6j5J4optMGe9iU",
                        "og:type": "website",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Google AdSense - Earn Money From Website Monetization",
                        "og:site_name": "Google AdSense",
                        "og:title": "Google AdSense - Earn Money From Website Monetization",
                        "og:description": "Earn money with website monetization from Google AdSense. We'll optimize your ad sizes to give them more chance to be seen and clicked.",
                        "twitter:image:src": "https://lh3.googleusercontent.com/ui3nbG7vgTq2sakCuph87K5w-nojUvsGkyR01xb6zoIosG0nzL5LuteUJ70yzil56lUeEmFWVHb1N3TZRgEAjU6j5J4optMGe9iU",
                        "referrer": "no-referrer",
                        "viewport": "initial-scale=1, minimum-scale=1, width=device-width",
                        "twitter:description": "Earn money with website monetization from Google AdSense. We'll optimize your ad sizes to give them more chance to be seen and clicked.",
                        "name": "Google AdSense - Earn Money From Website Monetization"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://lh3.googleusercontent.com/ui3nbG7vgTq2sakCuph87K5w-nojUvsGkyR01xb6zoIosG0nzL5LuteUJ70yzil56lUeEmFWVHb1N3TZRgEAjU6j5J4optMGe9iU"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Nonprofit Management Resources - Google for Nonprofits",
            "htmlTitle": "Nonprofit Management Resources - <b>Google</b> for Nonprofits",
            "link": "https://www.google.co.in/intl/en/nonprofits/",
            "displayLink": "www.google.co.in",
            "snippet": "Google's nonprofit management resources help you connect to the people who \nmatter, increase support, and raise greater awareness on a global stage.",
            "htmlSnippet": "<b>Google&#39;s</b> nonprofit management resources help you connect to the people who <br>\nmatter, increase support, and raise greater awareness on a global stage.",
            "cacheId": "TRQLAd1fZawJ",
            "formattedUrl": "https://www.google.co.in/intl/en/nonprofits/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/intl/en/nonprofits/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQmZVTf6zGB462V1LZYBuxkO4L5xgQQ4rHv4ac4lq5Xf3uZI-y1y-L8oXU",
                        "width": "310",
                        "height": "163"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://lh3.googleusercontent.com/OeG4W5OPZB6S1m9LBzWHY5RcfidqPTRcQNffWJfGVxvZyo97Ia6MpCxi1PPxZDWhAbB2VfysWP_amEy3jYeTFeld5JSrEuV0jfzRdA=w600-h314-rj",
                        "twitter:title": "Digital Tools for Nonprofits - Google for Nonprofits",
                        "twitter:site": "@googlenonprofit",
                        "viewport": "initial-scale=1, minimum-scale=1, width=device-width",
                        "twitter:description": "Make an impact with help from Google. Get access to digital tools to spread the world, collaborate with your team, fundraise and more.",
                        "og:title": "Digital Tools for Nonprofits - Google for Nonprofits",
                        "og:description": "Make an impact with help from Google. Get access to digital tools to spread the world, collaborate with your team, fundraise and more.",
                        "twitter:image": "https://lh3.googleusercontent.com/OeG4W5OPZB6S1m9LBzWHY5RcfidqPTRcQNffWJfGVxvZyo97Ia6MpCxi1PPxZDWhAbB2VfysWP_amEy3jYeTFeld5JSrEuV0jfzRdA=w600-h314-rj"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://lh3.googleusercontent.com/OeG4W5OPZB6S1m9LBzWHY5RcfidqPTRcQNffWJfGVxvZyo97Ia6MpCxi1PPxZDWhAbB2VfysWP_amEy3jYeTFeld5JSrEuV0jfzRdA=w600-h314-rj"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google Santa Tracker",
            "htmlTitle": "<b>Google</b> Santa Tracker",
            "link": "http://www.google.co.in/santatracker/",
            "displayLink": "www.google.co.in",
            "snippet": "Google serves cookies to analyse traffic to this site, and to optimize your \nexperience. Information about your use of our site is shared with Google for that \npurpose ...",
            "htmlSnippet": "<b>Google</b> serves cookies to analyse traffic to this site, and to optimize your <br>\nexperience. Information about your use of our site is shared with <b>Google</b> for that <br>\npurpose&nbsp;...",
            "cacheId": "P5srYqg-TyMJ",
            "formattedUrl": "www.google.co.in/santatracker/",
            "htmlFormattedUrl": "www.<b>google</b>.co.in/santatracker/",
            "pagemap": {
                "thumbnail": [
                    {
                        "src": "https://santatracker.google.com/images/og.png"
                    }
                ],
                "metatags": [
                    {
                        "og:type": "website",
                        "thumbnail": "https://santatracker.google.com/images/og.png",
                        "og:image:width": "1333",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Google Santa Tracker",
                        "twitter:site": "@google",
                        "viewport": "width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no,viewport-fit=cover",
                        "twitter:description": "Explore, play and learn with Santa's elves all December long",
                        "og:title": "Google Santa Tracker",
                        "og:image:height": "1000",
                        "twitter:image": "https://santatracker.google.com/images/og.png"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://santatracker.google.com/images/og.png",
                        "width": "259",
                        "type": "1",
                        "height": "194"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Programmable Search Engine by Google",
            "htmlTitle": "Programmable Search Engine by <b>Google</b>",
            "link": "https://www.google.co.in/cse",
            "displayLink": "www.google.co.in",
            "snippet": "Help people find what they need on your website. Add a customizable search \nbox to your web pages and show fast, relevant results powered by Google.",
            "htmlSnippet": "Help people find what they need on your website. Add a customizable search <br>\nbox to your web pages and show fast, relevant results powered by <b>Google</b>.",
            "cacheId": "b2M7_SLhvCcJ",
            "formattedUrl": "https://www.google.co.in/cse",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/cse",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTUEMtAoFZ82V2SGQOJTClsbvXUSjSFBKNXA91ggv-8GPA0BamdvhntmHxZ",
                        "width": "310",
                        "height": "163"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://programmablesearchengine.google.com/about/social_banner.jpg",
                        "og:type": "website",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Programmable Search Engine by Google",
                        "og:site_name": "Programmable Search Engine by Google",
                        "og:title": "Programmable Search Engine by Google",
                        "og:description": "Help people find what they need on your website. Add a customizable search box to your web pages and show fast, relevant results powered by Google.",
                        "twitter:creator": "@Google",
                        "og:image:secure_url": "https://programmablesearchengine.google.com/about/social_banner.jpg",
                        "twitter:image": "https://programmablesearchengine.google.com/about/social_twitter.jpg",
                        "referrer": "no-referrer",
                        "twitter:site": "@Google",
                        "viewport": "width=device-width, initial-scale=1.0",
                        "twitter:description": "Help people find what they need on your website. Add a customizable search box to your web pages and show fast, relevant results powered by Google.",
                        "og:locale": "en",
                        "og:url": "https://programmablesearchengine.google.com/about/"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://programmablesearchengine.google.com/about/social_banner.jpg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google: Privacy & Terms",
            "htmlTitle": "<b>Google</b>: Privacy &amp; Terms",
            "link": "https://www.google.co.in/policies/",
            "displayLink": "www.google.co.in",
            "snippet": "Visit safety.google to learn more about our built-in security, privacy controls, and \n... Your Google Account gives you quick access to settings and tools that let you ...",
            "htmlSnippet": "Visit safety.<b>google</b> to learn more about our built-in security, privacy controls, and <br>\n... Your <b>Google</b> Account gives you quick access to settings and tools that let you&nbsp;...",
            "cacheId": "8n3WeVq41L0J",
            "formattedUrl": "https://www.google.co.in/policies/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/policies/",
            "pagemap": {
                "metatags": [
                    {
                        "application-name": "Privacy & Terms – Google",
                        "referrer": "origin",
                        "apple-mobile-web-app-status-bar-style": "black",
                        "msapplication-tap-highlight": "no",
                        "viewport": "initial-scale=1, maximum-scale=5, width=device-width",
                        "apple-mobile-web-app-capable": "yes",
                        "apple-mobile-web-app-title": "Privacy & Terms – Google",
                        "mobile-web-app-capable": "yes"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Get More Customers With Easy Online Advertising | Google Ads",
            "htmlTitle": "Get More Customers With Easy Online Advertising | <b>Google</b> Ads",
            "link": "https://www.google.co.in/adwords/",
            "displayLink": "www.google.co.in",
            "snippet": "Grow your business with Google Ads. Get in front of customers when they're \nsearching for businesses like yours on Google Search and Maps. Only pay for \nresults, ...",
            "htmlSnippet": "Grow your business with <b>Google</b> Ads. Get in front of customers when they&#39;re <br>\nsearching for businesses like yours on <b>Google</b> Search and Maps. Only pay for <br>\nresults,&nbsp;...",
            "cacheId": "NmJsIYKPYtEJ",
            "formattedUrl": "https://www.google.co.in/adwords/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/adwords/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSp6HTLKSdybCwK-dbzcKnXBwSOWXFyki0EQkKJxNLRQOZgLWpbcuztgh8",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://lh3.googleusercontent.com/nupo3HWMIUeuul9r2IBSfpBo568bL-STG9nA71dUuW97DnhAXFgm2WWjczhTFqRHQZRf5VA-_PmxIZaIAXhOUrzfr5unPjFuW9za=w0",
                        "og:image:width": "1200",
                        "viewport": "initial-scale=1, minimum-scale=1, width=device-width",
                        "og:image:height": "1200"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://lh3.googleusercontent.com/nupo3HWMIUeuul9r2IBSfpBo568bL-STG9nA71dUuW97DnhAXFgm2WWjczhTFqRHQZRf5VA-_PmxIZaIAXhOUrzfr5unPjFuW9za=w0"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google Sky",
            "htmlTitle": "<b>Google</b> Sky",
            "link": "https://www.google.co.in/sky/",
            "displayLink": "www.google.co.in",
            "snippet": "Google Sky · Solar System · Constellations · Hubble Showcase · Backyard \nAstronomy · Chandra X-Ray Showcase · GALEX Ultraviolet Showcase · Spitzer \nInfrared ...",
            "htmlSnippet": "<b>Google</b> Sky &middot; Solar System &middot; Constellations &middot; Hubble Showcase &middot; Backyard <br>\nAstronomy &middot; Chandra X-Ray Showcase &middot; GALEX Ultraviolet Showcase &middot; Spitzer <br>\nInfrared&nbsp;...",
            "cacheId": "M07vsy8M2X0J",
            "formattedUrl": "https://www.google.co.in/sky/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/sky/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQsr9eTIxtYnl1X1bYkUfcbsVQwtHVPzfMLoZbhUAzlHuuS-MUFPzoLFAk",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://mw1.google.com/mw-planetary/sky/skytiles_v1/75_27_7.jpg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google Keep: Free Note Taking App for Personal Use",
            "htmlTitle": "<b>Google</b> Keep: Free Note Taking App for Personal Use",
            "link": "http://www.google.co.in/keep/",
            "displayLink": "www.google.co.in",
            "snippet": "Capture notes, share them with others, and access them from your computer, \nphone or tablet. Free with a Google account.",
            "htmlSnippet": "Capture notes, share them with others, and access them from your computer, <br>\nphone or tablet. Free with a <b>Google</b> account.",
            "cacheId": "yt6B_ILL5GoJ",
            "formattedUrl": "www.google.co.in/keep/",
            "htmlFormattedUrl": "www.<b>google</b>.co.in/keep/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcROKnZbFgp4uP1tPkxn8NhZD0ETpg2polSzZwicyS_0H4gtAhDRhwz4c5s",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "//www.google.com/images/icons/product/keep-512.png",
                        "viewport": "initial-scale=1, minimum-scale=1, width=device-width",
                        "og:title": "Meet Google Keep – Save your thoughts, wherever you are",
                        "og:url": "https://www.google.com/keep/",
                        "og:description": "Capture notes, share them with others, and access them from your computer, phone or tablet. Free with a Google account."
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.google.com/images/icons/product/keep-512.png"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google Chrome Web Browser",
            "htmlTitle": "<b>Google</b> Chrome Web Browser",
            "link": "https://www.google.co.in/chrome/",
            "displayLink": "www.google.co.in",
            "snippet": "Now more simple, secure and faster than ever – with Google's smarts built-in. \nDownload Chrome.",
            "htmlSnippet": "Now more simple, secure and faster than ever – with <b>Google&#39;s</b> smarts built-in. <br>\nDownload Chrome.",
            "cacheId": "1xjmkOCReYkJ",
            "formattedUrl": "https://www.google.co.in/chrome/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/chrome/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTRjERkm-H9sPK13aNKETxr25pxaNFgravJgL4SAUU0iKirHVhUMQ5Oig",
                        "width": "48",
                        "height": "48"
                    }
                ],
                "metatags": [
                    {
                        "msapplication-config": "/chrome/static/images/favicons/browserconfig.xml",
                        "og:image": "https://www.google.com/chrome/static/images/chrome-logo.svg",
                        "twitter:card": "summary_large_image",
                        "twitter:title": "Google Chrome Web Browser",
                        "og:type": "website",
                        "twitter:url": "https://www.google.com/intl/en_pk/chrome/",
                        "og:title": "Google Chrome Web Browser",
                        "og:description": "Now more simple, secure and faster than ever – with Google’s smarts.",
                        "twitter:image": "https://www.google.com/chrome/static/images/chrome-logo.svg",
                        "viewport": "width=device-width, initial-scale=1",
                        "twitter:description": "Now more simple, secure and faster than ever – with Google’s smarts.",
                        "og:locale": "en_PK",
                        "og:url": "https://www.google.com/intl/en_pk/chrome/"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://www.google.com/chrome/static/images/chrome-logo.svg"
                    }
                ]
            }
        },
        {
            "kind": "customsearch#result",
            "title": "Google My Business – Drive Customer Engagement on Google",
            "htmlTitle": "<b>Google</b> My Business – Drive Customer Engagement on <b>Google</b>",
            "link": "https://www.google.co.in/business/",
            "displayLink": "www.google.co.in",
            "snippet": "Engage with customers on Google for free. With a Google My Business Account, \nyou get more than a business listing. Your free Business Profile lets you easily ...",
            "htmlSnippet": "Engage with customers on <b>Google</b> for free. With a <b>Google</b> My Business Account, <br>\nyou get more than a business listing. Your free Business Profile lets you easily&nbsp;...",
            "cacheId": "ZL5pDNS6YdwJ",
            "formattedUrl": "https://www.google.co.in/business/",
            "htmlFormattedUrl": "https://www.<b>google</b>.co.in/business/",
            "pagemap": {
                "cse_thumbnail": [
                    {
                        "src": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9FtOIsMwirV0PdAn2OYxPtd_wxrQrwtgkNrltDNALmbA7Ykf9Xy5gitI",
                        "width": "225",
                        "height": "225"
                    }
                ],
                "metatags": [
                    {
                        "og:image": "https://lh3.googleusercontent.com/g6m_L_b0jkjw4B_cDV1zZi99B9Mb04OVRbGVPDEt8Ga13YdYeg8lPC5KiuxgqgeoB0N-X6gwGgMmfkpYgjzKddFQRrniKIEpghmdIQ=w0",
                        "twitter:title": "Google My Business – Drive Customer Engagement on Google",
                        "og:image:width": "1200",
                        "twitter:card": "summary",
                        "viewport": "initial-scale=1, minimum-scale=1, width=device-width",
                        "twitter:url": "https://www.google.com/intl/en_in/business/",
                        "twitter:description": "Your free Business Profile on Google My Business helps you drive customer engagement with local customers across Google Search and Maps.",
                        "og:title": "Google My Business – Drive Customer Engagement on Google",
                        "og:image:height": "1200",
                        "og:url": "https://www.google.com/intl/en_in/business/",
                        "og:description": "Your free Business Profile on Google My Business helps you drive customer engagement with local customers across Google Search and Maps.",
                        "twitter:image": "https://lh3.googleusercontent.com/g6m_L_b0jkjw4B_cDV1zZi99B9Mb04OVRbGVPDEt8Ga13YdYeg8lPC5KiuxgqgeoB0N-X6gwGgMmfkpYgjzKddFQRrniKIEpghmdIQ"
                    }
                ],
                "cse_image": [
                    {
                        "src": "https://lh3.googleusercontent.com/g6m_L_b0jkjw4B_cDV1zZi99B9Mb04OVRbGVPDEt8Ga13YdYeg8lPC5KiuxgqgeoB0N-X6gwGgMmfkpYgjzKddFQRrniKIEpghmdIQ=w0"
                    }
                ]
            }
        }
    ]
}