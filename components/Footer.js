function Footer() {
    return (
        <footer className="grid w-full divide-y-[1px] bg-gray-100">
            <div className="px-4 py-2 md:px-8 md:py-3 text-gray-400">
                <p>India</p>
            </div>
            <div className="text-gray-500 grid grid-cols-1 md:grid-cols-2 text-sm">
                <div className="flex justify-evenly md:space-x-8 md:justify-start md:ml-[10%] lg:ml-8 py-3 whitespace-nowrap">
                    <p>About</p>  
                    <p>Advertising</p>
                    <p>Business</p>
                    <p>How Search Works</p>
                </div>
                <div className="flex space-x-4 md:space-x-8 justify-center py-3 md:mr-[10%] lg:mr-8 md:ml-auto">
                    <p>Privacy</p>  
                    <p>Terms</p>
                    <p>Settings</p>
                </div>
            </div>
        </footer>
    )
}

export default Footer
