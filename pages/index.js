import Head from 'next/head'
import Avatar from '../components/Avatar'
import {SearchIcon,ViewGridIcon} from '@heroicons/react/solid'
import Image from 'next/image'
import Footer from '../components/Footer'
import {useRef} from 'react'
import {useRouter} from 'next/router'

export default function Home() {
  const router=useRouter();
  const searchInputRef = useRef(null);
  const search = (e) => {
    e.preventDefault();
    const term=searchInputRef.current.value;
    if(!term) return;
    router.push(`/search?term=${term}`);
  };

  return (
    <div className="flex flex-col h-screen">
      <Head>
        <title>Google</title>
        <link rel="icon" href="/search.svg" />
      </Head>

    <header className="w-full flex justify-end p-3 md:p-5">
      <div className="flex space-x-2 md:space-x-4 text-sm text-gray-700 items-center">
        <p className="link">Gmail</p>
        <p className="link">Images</p>
        <img src="/dots-menu.svg" className="h-10 p-3 rounded-full hover:bg-gray-200"/>
        <Avatar url="https://scontent.fgau1-1.fna.fbcdn.net/v/t1.6435-9/168315799_783172012613923_7211455786272834843_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=NhZVEbED6-oAX-KM8ej&_nc_ht=scontent.fgau1-1.fna&oh=d8aaa46fd787d4989020b55390dc938a&oe=609CD8A5"/>
      </div>
    </header>
    <form className="flex flex-col items-center p-10 md:p-0 flex-grow mt-20 md:mt-44">
      <img className="h-14 md:h-24" src="/google-logo.svg"/>
      <div className="flex w-full mt-5 px-5 py-3 items-center hover:shadow-lg focus-within:shadow-lg max-w-md rounded-full border border-gray-200 sm:max-w-xl lg:max-w-2xl">
        <SearchIcon className="h-5 mr-3 text-gray-400" />
        <input ref={searchInputRef} type="text" className="focus:outline-none flex-grow" />
        <img src="/google-mic.svg" className="h-5" />
      </div>
      <div className="flex mt-8 space-x-2">
        <button onClick={search} className="btn">Google Search</button>
        <button onClick={search} className="btn">I'm Feeling Lucky</button>
      </div>
    </form>
    <Footer/>
    </div>
  )
}
