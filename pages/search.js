import Head from 'next/head'
import { useRouter } from 'next/router'
import Header from '../components/Header'
import SearchResults from '../components/SearchResults'
import Response from '../Response'

function search({results}) {
    console.log(results)
    const router=useRouter();
    return (
        <div>
            <Head>
                <title>{router.query.term} - Google Search</title>
                <link rel="icon" href="/search.svg"/>
            </Head>
            <Header/>
            <SearchResults results={results}/>
        </div>
    )
}

export default search

export async function getServerSideProps(context) {
    const startIndex=context.query.start || '0';
    const data= Response;
    return {
        props: {
            results: data
        }
    }
}