function Avatar({url}) {
    return (
        <img
            loading="lazy"
            className="h-10 rounded-full border-4 border-white hover:border-gray-200 hover:cursor-pointer"
            src={url}
            alt="profile picture"
        />
    )
}

export default Avatar
