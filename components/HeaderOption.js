function HeaderOption({Icon,title,selected}) {
    return (
        <div className={`flex items-center space-x-1 border-b-3 border-transparent hover:text-blue-700 cursor-pointer hover:border-blue-600 pb-2 ${selected && "text-blue-700 border-blue-600"}`}>
            <Icon className="h-4"/> 
            <p className="hidden sm:inline-flex">{title}</p>
        </div>
    )
}

export default HeaderOption
