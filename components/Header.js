import {SearchIcon, XIcon } from '@heroicons/react/outline';
import Image from 'next/image'
import {useRouter} from 'next/router'
import {useRef} from 'react'
import Avatar from '../components/Avatar'
import HeaderOptions from '../components/HeaderOptions'

function Header() {
    const router=useRouter();
    const searchInputRef=useRef(null);
    const search=(e)=>{
        e.preventDefault();
        const term=searchInputRef.current.value;
        if(!term) return;
        router.push(`/search?term=${term}`);
    };
    return (
        <header className="sticky top-0 bg-white">
            <div className="flex w-full p-4 md:p-6 items-center">
                <Image
                    src="/google-logo.svg"
                    height={30}
                    width={90}
                    className="cursor-pointer"
                    onClick={()=>router.push("/")}
                />
                <form className="flex flex-grow px-6 py-3 ml-10 mr-5 border shadow-md rounded-full hover:shadow-lg max-w-3xl items-center">
                    <input className="flex-grow w-full focus:outline-none" ref={searchInputRef} defaultValue={router.query.term} type="text"></input>
                    <XIcon className="h-6 text-gray-500 cursor-pointer" onClick={()=>searchInputRef.current.value=""}/>
                    <img className="h-5 mx-3 pl-3 border-l border-gray-300 cursor-pointer" src="/google-mic.svg"/>
                    <SearchIcon onClick={search} className="h-6 ml-2 text-blue-500 cursor-pointer"/>
                    <button hidden type="submit" onClick={search}></button>
                </form>
                <div className="ml-auto">
                    <Avatar url="https://scontent.fgau1-1.fna.fbcdn.net/v/t1.6435-9/168315799_783172012613923_7211455786272834843_n.jpg?_nc_cat=104&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=NhZVEbED6-oAX-KM8ej&_nc_ht=scontent.fgau1-1.fna&oh=d8aaa46fd787d4989020b55390dc938a&oe=609CD8A5"/>
                </div>
            </div>
            <HeaderOptions/>
        </header>
    )
}

export default Header
