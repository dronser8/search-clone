import PaginationButtons from "./PaginationButtons"

function SearchResults({results}) {
    return (
        <div>
            <div className="mx-auto w-full px-3 pl-[11%] md:pl-[12%] lg:pl-44">
                <p className="text-gray-500 text-md mb-5 mt-3">About {results.searchInformation?.formattedTotalResults} results ({results.searchInformation?.formattedSearchTime}) seconds</p>
                {results.items?.map((result) => (
                    <div key={result.link} className="max-w-xl mb-8">
                        <div className="group">
                            <a href={result.link} className="text-sm">{result.formattedUrl}</a>
                            <a href={result.link}>
                                <h2 className="truncate text-xl font-medium text-blue-800 group-hover:underline">{result.title}</h2>
                            </a>
                        </div>
                        <p className="line-clamp-2">{result.snippet}</p>
                    </div>
                ))}
            </div>
            <div className="md:max-w-3xl lg:max-w-4xl flex justify-between">
                <PaginationButtons/>
            </div>  
        </div>
        
    )
}

export default SearchResults
